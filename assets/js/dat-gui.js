// https://observablehq.com/@d3/color-schemes

// Color function (color_function)
var colorRainbow = function(x, y) {
	return "hsl(" + Math.floor(Math.abs(x * y) * 360) + ",80%,60%)";
};

const getPalette = function(name) {
	const palettes = {
		random: 'random',
		blues: ["#f7fbff","#f6faff","#f5fafe","#f5f9fe","#f4f9fe","#f3f8fe","#f2f8fd","#f2f7fd","#f1f7fd","#f0f6fd","#eff6fc","#eef5fc","#eef5fc","#edf4fc","#ecf4fb","#ebf3fb","#eaf3fb","#eaf2fb","#e9f2fa","#e8f1fa","#e7f1fa","#e7f0fa","#e6f0f9","#e5eff9","#e4eff9","#e3eef9","#e3eef8","#e2edf8","#e1edf8","#e0ecf8","#e0ecf7","#dfebf7","#deebf7","#ddeaf7","#ddeaf6","#dce9f6","#dbe9f6","#dae8f6","#d9e8f5","#d9e7f5","#d8e7f5","#d7e6f5","#d6e6f4","#d6e5f4","#d5e5f4","#d4e4f4","#d3e4f3","#d2e3f3","#d2e3f3","#d1e2f3","#d0e2f2","#cfe1f2","#cee1f2","#cde0f1","#cce0f1","#ccdff1","#cbdff1","#cadef0","#c9def0","#c8ddf0","#c7ddef","#c6dcef","#c5dcef","#c4dbee","#c3dbee","#c2daee","#c1daed","#c0d9ed","#bfd9ec","#bed8ec","#bdd8ec","#bcd7eb","#bbd7eb","#b9d6eb","#b8d5ea","#b7d5ea","#b6d4e9","#b5d4e9","#b4d3e9","#b2d3e8","#b1d2e8","#b0d1e7","#afd1e7","#add0e7","#acd0e6","#abcfe6","#a9cfe5","#a8cee5","#a7cde5","#a5cde4","#a4cce4","#a3cbe3","#a1cbe3","#a0cae3","#9ec9e2","#9dc9e2","#9cc8e1","#9ac7e1","#99c6e1","#97c6e0","#96c5e0","#94c4df","#93c3df","#91c3df","#90c2de","#8ec1de","#8dc0de","#8bc0dd","#8abfdd","#88bedc","#87bddc","#85bcdc","#84bbdb","#82bbdb","#81badb","#7fb9da","#7eb8da","#7cb7d9","#7bb6d9","#79b5d9","#78b5d8","#76b4d8","#75b3d7","#73b2d7","#72b1d7","#70b0d6","#6fafd6","#6daed5","#6caed5","#6badd5","#69acd4","#68abd4","#66aad3","#65a9d3","#63a8d2","#62a7d2","#61a7d1","#5fa6d1","#5ea5d0","#5da4d0","#5ba3d0","#5aa2cf","#59a1cf","#57a0ce","#569fce","#559ecd","#549ecd","#529dcc","#519ccc","#509bcb","#4f9acb","#4d99ca","#4c98ca","#4b97c9","#4a96c9","#4895c8","#4794c8","#4693c7","#4592c7","#4492c6","#4391c6","#4190c5","#408fc4","#3f8ec4","#3e8dc3","#3d8cc3","#3c8bc2","#3b8ac2","#3a89c1","#3988c1","#3787c0","#3686c0","#3585bf","#3484bf","#3383be","#3282bd","#3181bd","#3080bc","#2f7fbc","#2e7ebb","#2d7dbb","#2c7cba","#2b7bb9","#2a7ab9","#2979b8","#2878b8","#2777b7","#2676b6","#2574b6","#2473b5","#2372b4","#2371b4","#2270b3","#216fb3","#206eb2","#1f6db1","#1e6cb0","#1d6bb0","#1c6aaf","#1c69ae","#1b68ae","#1a67ad","#1966ac","#1865ab","#1864aa","#1763aa","#1662a9","#1561a8","#1560a7","#145fa6","#135ea5","#135da4","#125ca4","#115ba3","#115aa2","#1059a1","#1058a0","#0f579f","#0e569e","#0e559d","#0e549c","#0d539a","#0d5299","#0c5198","#0c5097","#0b4f96","#0b4e95","#0b4d93","#0b4c92","#0a4b91","#0a4a90","#0a498e","#0a488d","#09478c","#09468a","#094589","#094487","#094386","#094285","#094183","#084082","#083e80","#083d7f","#083c7d","#083b7c","#083a7a","#083979","#083877","#083776","#083674","#083573","#083471","#083370","#08326e","#08316d","#08306b"],
		greens: ["#f7fcf5","#f6fcf4","#f6fcf4","#f5fbf3","#f5fbf2","#f4fbf2","#f4fbf1","#f3faf0","#f2faf0","#f2faef","#f1faee","#f1faee","#f0f9ed","#f0f9ec","#eff9ec","#eef9eb","#eef8ea","#edf8ea","#ecf8e9","#ecf8e8","#ebf7e7","#ebf7e7","#eaf7e6","#e9f7e5","#e9f6e4","#e8f6e4","#e7f6e3","#e7f6e2","#e6f5e1","#e5f5e1","#e4f5e0","#e4f4df","#e3f4de","#e2f4dd","#e1f4dc","#e1f3dc","#e0f3db","#dff3da","#def2d9","#ddf2d8","#ddf2d7","#dcf1d6","#dbf1d5","#daf1d4","#d9f0d3","#d8f0d2","#d7efd1","#d6efd0","#d5efcf","#d4eece","#d4eece","#d3eecd","#d2edcb","#d1edca","#d0ecc9","#cfecc8","#ceecc7","#cdebc6","#ccebc5","#cbeac4","#caeac3","#c9eac2","#c8e9c1","#c6e9c0","#c5e8bf","#c4e8be","#c3e7bd","#c2e7bc","#c1e6bb","#c0e6b9","#bfe6b8","#bee5b7","#bde5b6","#bbe4b5","#bae4b4","#b9e3b3","#b8e3b2","#b7e2b0","#b6e2af","#b5e1ae","#b3e1ad","#b2e0ac","#b1e0ab","#b0dfaa","#aedfa8","#addea7","#acdea6","#abdda5","#aadca4","#a8dca3","#a7dba2","#a6dba0","#a5da9f","#a3da9e","#a2d99d","#a1d99c","#9fd89b","#9ed799","#9dd798","#9bd697","#9ad696","#99d595","#97d494","#96d492","#95d391","#93d390","#92d28f","#91d18e","#8fd18d","#8ed08c","#8ccf8a","#8bcf89","#8ace88","#88cd87","#87cd86","#85cc85","#84cb84","#82cb83","#81ca82","#80c981","#7ec980","#7dc87f","#7bc77e","#7ac77c","#78c67b","#77c57a","#75c479","#74c478","#72c378","#71c277","#6fc276","#6ec175","#6cc074","#6bbf73","#69bf72","#68be71","#66bd70","#65bc6f","#63bc6e","#62bb6e","#60ba6d","#5eb96c","#5db86b","#5bb86a","#5ab769","#58b668","#57b568","#56b467","#54b466","#53b365","#51b264","#50b164","#4eb063","#4daf62","#4caf61","#4aae61","#49ad60","#48ac5f","#46ab5e","#45aa5d","#44a95d","#42a85c","#41a75b","#40a75a","#3fa65a","#3ea559","#3ca458","#3ba357","#3aa257","#39a156","#38a055","#379f54","#369e54","#359d53","#349c52","#339b51","#329a50","#319950","#30984f","#2f974e","#2e964d","#2d954d","#2b944c","#2a934b","#29924a","#28914a","#279049","#268f48","#258f47","#248e47","#238d46","#228c45","#218b44","#208a43","#1f8943","#1e8842","#1d8741","#1c8640","#1b8540","#1a843f","#19833e","#18823d","#17813d","#16803c","#157f3b","#147e3a","#137d3a","#127c39","#117b38","#107a37","#107937","#0f7836","#0e7735","#0d7634","#0c7534","#0b7433","#0b7332","#0a7232","#097131","#087030","#086f2f","#076e2f","#066c2e","#066b2d","#056a2d","#05692c","#04682b","#04672b","#04662a","#03642a","#036329","#026228","#026128","#026027","#025e27","#015d26","#015c25","#015b25","#015a24","#015824","#015723","#005623","#005522","#005321","#005221","#005120","#005020","#004e1f","#004d1f","#004c1e","#004a1e","#00491d","#00481d","#00471c","#00451c","#00441b"],
	};
	return palettes[name];
};


var trianglifySettings = {
	cell_size: 75,
	variance: 0.1,
	seed: Math.floor(Math.random() * Math.floor(2147483647)),
	color_space: "rgb",
	x_colors: "random",
	y_colors: "random"
};

var settings = {};
var pattern = null;

var GUIsettings = {
	background: "#000000",
	x_color_a: "#ff00ff",
	x_color_b: "#ff00ff",
	y_color_a: "#ff00ff",
	y_color_b: "#ff00ff",
	exportSVG: function(){
		 exportSVG()
	}
};


// Function to download data to a file
const download = function(data, filename, type) {
	var file = new Blob([data], {type: type});
	if (window.navigator.msSaveOrOpenBlob) // IE10+
			window.navigator.msSaveOrOpenBlob(file, filename);
	else { 
		var a = document.createElement("a"), 
		url = URL.createObjectURL(file);
		a.href = url;
		a.download = filename;
		document.body.appendChild(a);
		a.click();
		setTimeout(function() {
				document.body.removeChild(a);
				window.URL.revokeObjectURL(url);  
		}, 0); 
	}
}

const exportSVG = function() {
	let today = new Date();
	let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	let dateTime = date+' '+time;

	let svg = pattern.svg();
	svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
	download(svg.outerHTML, `trianglify-${dateTime}.svg`, 'image/svg+xml');
};


const generate = function(trianglifySettings) {

	trianglifySettings.width= window.innerWidth;
	trianglifySettings.height= window.innerHeight;

	console.info("pattern settings", trianglifySettings);
	console.info("GUIsettings", GUIsettings);

	pattern = Trianglify(trianglifySettings);

	let container = document.querySelector(".container");
	container.innerHTML = "";
	container.appendChild(pattern.canvas());
};


window.onload = function() {
	var body = document.body;

	var gui = new dat.GUI();
	// gui.remember(trianglifySettings);

	// setup controllers
	settings.cell_size = gui.add(trianglifySettings, "cell_size", 10, 1000);
	settings.variance = gui.add(trianglifySettings, "variance", 0.0, 10);
	settings.seed = gui.add(trianglifySettings, "seed", 0, 2147483647);
	// settings.x_colors = gui.add(trianglifySettings, "x_colors", ["random", "blues", "greens"]);

	var folderColors = gui.addFolder("Colors");
	settings.background = folderColors.addColor(GUIsettings, "background");
	settings.x_color_a = folderColors.addColor(GUIsettings, "x_color_a");
	settings.x_color_b = folderColors.addColor(GUIsettings, "x_color_b");
	settings.y_color_a = folderColors.addColor(GUIsettings, "y_color_a");
	settings.y_color_b = folderColors.addColor(GUIsettings, "y_color_b");

	var folderExport = gui.addFolder("Export");
	folderExport.add(GUIsettings, "exportSVG");


	settings.cell_size.onChange(function(value) {
		generate(trianglifySettings);
	});

	settings.variance.onChange(function(value) {
		generate(trianglifySettings);
	});

	settings.seed.onChange(function(value) {
		generate(trianglifySettings);
	});

	settings.background.onChange(function(value) {
		document.body.style.setProperty("background-color", value);
	});

	settings.x_color_a.onChange(function(value) {
		trianglifySettings.x_colors = new Array(value, GUIsettings.x_color_b);
		generate(trianglifySettings);
	});
	settings.x_color_b.onChange(function(value) {
		trianglifySettings.x_colors = new Array(GUIsettings.x_color_a, value);
		generate(trianglifySettings);
	});

	settings.y_color_a.onChange(function(value) {
		trianglifySettings.y_colors = new Array(value, GUIsettings.y_color_b);
		generate(trianglifySettings);
	});
	settings.y_color_b.onChange(function(value) {
		trianglifySettings.y_colors = new Array(GUIsettings.y_color_a, value);
		generate(trianglifySettings);
	});

	// first paint
	console.log(trianglifySettings);
	generate(trianglifySettings);
};
